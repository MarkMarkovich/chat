import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.*;


public class SimpleChatClient {

	JTextArea incoming;
	JTextField outgoing;
	BufferedReader reader;
	PrintWriter writer;
	Socket sock;
	
	
	public static void main(String[] args) {
		SimpleChatClient client = new SimpleChatClient();
		client.go();
	}//end of main

	private void go() {
		JFrame frame = new JFrame("Chat client");
		JPanel mainPanel = new JPanel();
		incoming = new JTextArea(15,50);
		incoming.setLineWrap(true);
		incoming.setEditable(false);
		incoming.setWrapStyleWord(true);
			JScrollPane qScroller = new JScrollPane(incoming);
			qScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			qScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		outgoing = new JTextField(20);
		JButton sendButton = new JButton("SEND THIS SHIT");
		sendButton.addActionListener(new SendButtonListener());
			mainPanel.add(qScroller);
			mainPanel.add(outgoing);
			mainPanel.add(sendButton);
	setUpNetworking();
			Thread readerThread = new Thread(new IncomingReader());
	readerThread.start();
			frame.getContentPane().add(BorderLayout.CENTER, mainPanel);
			frame.setSize(400, 500);
			frame.setVisible(true);
	}//end of go method

	private void setUpNetworking() {
		try {
			sock = new Socket("192.168.110.62", 5001);
			InputStreamReader streamReader = new InputStreamReader(sock.getInputStream());
			reader = new BufferedReader(streamReader);
			writer = new PrintWriter(sock.getOutputStream());
			System.out.println("NETWORK OK!!!");
			
		} catch (IOException ex) {ex.printStackTrace();}
		
	}//end of setUpNetworking method

	
	public class SendButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			try{
			writer.println(outgoing.getText());
			
			writer.flush();
			}catch(Exception ex){ex.printStackTrace();}
			outgoing.setText("");
			outgoing.requestFocus();
		}//end of actionPerformed method

	}//end of SendButtonListener class

	
	public class IncomingReader implements Runnable{
		public void run() {
			String message;
			try{
				while((message = reader.readLine()) !=null){
					System.out.println("read" + message);
					incoming.append(message + "\n");
				}
			}catch(Exception ex){ex.printStackTrace();}
			
		}//end of run method
		
	}//end of class IncomingReader
	
}//end of SimpleChatClient class
