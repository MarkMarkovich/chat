import java.net.*;
import java.io.*;
import java.util.*;

public class VerySimpleChatServer {
	
	ArrayList clientOutputStream;
	
	public class ClientHandler implements Runnable{
			BufferedReader reader;
			Socket sock;
			
		public ClientHandler(Socket clientSocket){
			try{
				sock = clientSocket;
				InputStreamReader isr = new InputStreamReader(sock.getInputStream());
				reader = new BufferedReader(isr);
			}catch(Exception ex){ex.printStackTrace();}
		}//constructor ClientHandler
		
		
		
		public void run() {
			String massage;
			try{
				while((massage = reader.readLine()) !=null){
					tellEveryone(massage);
				}			
			}catch(Exception ex){ex.printStackTrace();}
			
		}



	private void tellEveryone(String massage) {
			Iterator it = clientOutputStream.iterator();
			while(it.hasNext()){
				try{
					PrintWriter writer = (PrintWriter) it.next();
					writer.println(massage);
					writer.flush();
				}catch(Exception ex){ex.printStackTrace();}
			}//end of while
		}
		
	}//class ClientHandler
	
	public static void main(String[] args) {
		new VerySimpleChatServer().go();

	}

	public void go(){
			clientOutputStream = new ArrayList();
		try{
			ServerSocket serverSock = new ServerSocket(5001);
			while(true){
				Socket clientSocet = serverSock.accept();
				PrintWriter writer = new PrintWriter(clientSocet.getOutputStream());
				clientOutputStream.add(writer);
				
				Thread t = new Thread(new ClientHandler(clientSocet));
				t.start();
				System.out.println("got a connection");
			}
		}catch(Exception ex){ex.printStackTrace();}
	}//end of go()
	
}//class VerySimpleChatServer









